import {
  productsSearch,
  productFeched,
  search,
  originalProductsFeched,
  sort,
  productsSort,
  priceFilterValue,
  priceMinMax,
  cartProducts,
  checkedProductsArr,
  checkProduct,
  uncheckProduct,
  modalOpen,
} from "./Types";
const initialState = {
  originalProducts: [],
  catalogProductsList: [],
  searchQuery: "",
  sortValue: "",
  priceMinMaxArr: [],
  filterPrice: [],
  cartProductsList: [],
  checkedProducts: [],
  modal: false,
};

const CatalogPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case originalProductsFeched:
      return { ...state, originalProducts: action.payload };
    case productsSearch:
    case productFeched:
    case productsSort:
      return { ...state, catalogProductsList: action.payload };
    case search:
      return { ...state, searchQuery: action.payload };
    case sort:
      return { ...state, sortValue: action.payload };
    case priceFilterValue:
      return { ...state, filterPrice: action.payload };
    case priceMinMax:
      return { ...state, priceMinMaxArr: action.payload };
    case cartProducts:
      return { ...state, cartProductsList: action.payload };
    case checkedProductsArr:
      return { ...state, checkedProducts: action.payload };
    case modalOpen:
      return { ...state, modal: action.payload };
    case checkProduct:
      return {
        ...state,
        checkedProducts: [...state.checkedProducts, action.payload],
      };
    case uncheckProduct:
      return {
        ...state,
        checkedProducts: [
          ...state.checkedProducts.filter((item) => item !== action.payload),
        ],
      };
    default:
      return state;
  }
};

export default CatalogPageReducer;
