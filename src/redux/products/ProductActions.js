import {
  productFeched,
  originalProductsFeched,
  search,
  productsSearch,
  sort,
  productsSort,
  priceFilterValue,
  priceMinMax,
  cartProducts,
  checkedProductsArr,
  checkProduct,
  uncheckProduct,
  modalOpen,
} from "./Types";

export const originalProductlist = (arr) => {
  return {
    type: originalProductsFeched,
    payload: arr,
  };
};
export const catalogItemsRequestAction = (arr) => {
  return {
    type: productFeched,
    payload: arr,
  };
};
export const productSearch = (query) => {
  return {
    type: productsSearch,
    payload: query,
  };
};
export const searchQuery = (query) => {
  return {
    type: search,
    payload: query,
  };
};
export const sortValueAction = (query) => {
  return {
    type: sort,
    payload: query,
  };
};
export const productsSortAction = (sortedArr) => {
  return {
    type: productsSort,
    payload: sortedArr,
  };
};
export const filterPriceAction = (value) => {
  return {
    type: priceFilterValue,
    payload: value,
  };
};
export const filterPriceMinMaxAction = (value) => {
  return {
    type: priceMinMax,
    payload: value,
  };
};

export const cartProductsListAction = (arr) => {
  return {
    type: cartProducts,
    payload: arr,
  };
};
export const checkedProductsAction = (arr) => {
  return {
    type: checkedProductsArr,
    payload: arr,
  };
};

export const checkProductAction = (id) => {
  return {
    type: checkProduct,
    payload: id,
  };
};
export const uncheckAction = (id) => {
  return {
    type: uncheckProduct,
    payload: id,
  };
};
export const modalAction = (boolean) => {
  return {
    type: modalOpen,
    payload: boolean,
  };
};
