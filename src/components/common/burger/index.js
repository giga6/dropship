import "./style.css";
import React from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import MenuIcon from "@material-ui/icons/Menu";
import useMediaQuery from "@material-ui/core/useMediaQuery";

const useStyles = makeStyles({
  fullList: {
    width: "auto",
  },
  burger: {
    width: "40px",
    height: "50px",
    color: "#61d5df",
  },
  burgerButton: {
    display: "none",
  },
  burgerList: {
    marginTop: "75px",
  },
});
const Burger = ({ burgerList }) => {
  const classes = useStyles();
  const matches = useMediaQuery("(max-width:880px)");
  const [state, setState] = React.useState({
    top: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === "top" || anchor === "bottom",
      })}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        {burgerList.map((text, index) => (
          <ListItem button key={text}>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
    </div>
  );
  return (
    <div>
      {["top"].map((anchor) => (
        <React.Fragment key={anchor}>
          {matches && (
            <Button onClick={toggleDrawer(anchor, true)}>
              <MenuIcon className={classes.burger}></MenuIcon>
            </Button>
          )}
          <Drawer
            className={classes.burgerList}
            anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
          >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
};

export default Burger;
