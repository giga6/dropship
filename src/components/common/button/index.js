import "./button.css";
const Button = ({ title, className }) => {
  return (
    <button id="button" className={className}>
      {title}
    </button>
  );
};
export default Button;
