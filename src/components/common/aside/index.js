import "./Aside.css";
import { Link } from "react-router-dom";
import logo from "../../../assets/dropship_logo.png";
import profileIcon from "../../../assets/profile-example.jpg";
import dashboarIcon from "../../../assets/dashboard.svg";
import catalogIcon from "../../../assets/catalog.svg";
import inventoryIcon from "../../../assets/inventory.svg";
import cartIcon from "../../../assets/cart.svg";
import orderIcon from "../../../assets/order.svg";
import transactionsIcon from "../../../assets/transactions.svg";
import storeIcon from "../../../assets/store.svg";

const Aside = () => {
  return (
    <nav className="aside_nav">
      <div className="aside_logo">
        <img alt="logo" src={logo}></img>
      </div>
      <ul className="aside_ul">
        <Link to="/profile">
          <li className="profile_icon">
            <img alt="profileIcon" src={profileIcon}></img>
          </li>
        </Link>
        <Link to="/dashboard">
          <li className="aside_icon">
            <img alt="dashboarIcon" src={dashboarIcon}></img>
          </li>
        </Link>
        <Link to="/catalog">
          <li className="aside_icon">
            <img alt="catalogIcon" src={catalogIcon}></img>
          </li>
        </Link>
        <Link to="/inventory">
          <li className="aside_icon">
            <img alt="inventoryIcon" src={inventoryIcon}></img>
          </li>
        </Link>
        <Link to="/cart">
          <li className="aside_icon">
            <img alt="cartIcon" src={cartIcon}></img>
          </li>
        </Link>
        <Link to="/order">
          <li className="aside_icon">
            <img src={orderIcon}></img>
          </li>
        </Link>
        <Link to="/transactions">
          <li className="aside_icon">
            <img alt="transactionsIcon" src={transactionsIcon}></img>
          </li>
        </Link>
        <Link to="/store">
          <li className="aside_icon">
            <img alt="storeIcon" src={storeIcon}></img>
          </li>
        </Link>
      </ul>
    </nav>
  );
};

export default Aside;
