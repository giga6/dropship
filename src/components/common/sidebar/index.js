import React from "react";
import "./sidebar.css";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { filterPriceAction } from "../../../redux/products/ProductActions";

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  slider: {
    color: "#5D6B9F",
  },
  slierTitle: {
    fontSize: "12px",
    color: "black",
  },
});

const Sidebar = () => {
  const classes = useStyles();
  const prices = useSelector((state) => state.ProductReducers.filterPrice);
  const filterPriceValue = [Math.min(...prices), Math.max(...prices)];
  const productList = useSelector(
    (state) => state.ProductReducers.originalProducts
  );
  const dispatch = useDispatch();

  const handlePriceChange = (event, newValue) => {
    setTimeout(() => {
      dispatch(filterPriceAction(newValue), 2000);
    });
  };

  return (
    <div className="sidebar_wrapper">
      <div className="sidebar_top">
        <div className="dropdown_niche">
          <p className="dropdown_categories-p">Choose Niche</p>
        </div>
        <div className="dropdown_categories">
          <p className="dropdown_categories-p">Choose Category</p>
        </div>
      </div>
      <div className="sidebar-bottom-wrapper">
        <div className="sidebar-bottom">
          <div className="dropdown_ship">
            <p>Ship from</p>
          </div>
          <div className="dropdown_ship">
            <p>Ship to</p>
          </div>
          <div className="dropdown_ship">
            <p>Ship from</p>
          </div>
          <div className={classes.root}>
            <Typography
              className={classes.slierTitle}
              id="range-slider"
              gutterBottom
            >
              PRICE RANGE
            </Typography>
            <Slider
              value={filterPriceValue}
              onChange={handlePriceChange}
              aria-labelledby="range-slider"
              className={classes.slider}
              min={12}
              max={25000}
            />
            <div className="price_div">
              <div className="min_price">
                <div className="price_valute">$</div>
                <div className="price_value">{filterPriceValue[0]}</div>
              </div>
              <div className="max_price">
                <div className="price_valute">$</div>
                <div className="price_value">{filterPriceValue[1]}</div>
              </div>
            </div>
          </div>
          {/* <div className={classes.root}>
            <Typography
              className={classes.slierTitle}
              id="range-slider"
              gutterBottom
            >
              PROFIT RANGE
            </Typography>
            <Slider
              value={value}
              onChange={handleChange}
              valueLabelDisplay="auto"
              aria-labelledby="range-slider"
              getAriaValueText={valuetext}
              className={classes.slider}
            />
          </div> */}
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
