import "./catalogItem.css";
import React, { useEffect } from "react";
import { Grid } from "@material-ui/core";
import { addToCart, editProduct, deleteProduct, products } from "../../../API";
import { useSnackbar } from "notistack";
import { useSelector } from "react-redux";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { makeStyles } from "@material-ui/core";
import { useDispatch } from "react-redux";
import {
  catalogItemsRequestAction,
  originalProductlist,
} from "../../../redux/products/ProductActions";
import { useFormik } from "formik";
import { useState } from "react";
import { modalAction } from "../../../redux/products/ProductActions";
import { useHistory } from "react-router-dom";
import ProductModal from "./Modal";
const useStyles = makeStyles({
  editIcon: {
    display: "none",
  },
  editIconActive: {
    backgroundColor: "#61d5df",
    color: "white",
    display: "block",
    padding: "6px",
    borderRadius: "3px",
  },
});

const CatalogItem = ({ item, selectItem, productList }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const checkedProducts = useSelector(
    (state) => state.ProductReducers.checkedProducts
  );
  const userInfo = JSON.parse(localStorage.getItem("user"));
  const isAdmin = userInfo?.data.data.isAdmin;
  const [open, setOpen] = useState(false);
  const history = useHistory();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const formik = useFormik({
    initialValues: {
      title: item.title,
      description: item.description,
      price: item.price,
      imageUrl: item.imageUrl,
    },
    onSubmit: async (data) => {
      await editProduct(item.id, data);
      await handleClose();
      products().then((res) => {
        dispatch(catalogItemsRequestAction(res));
        dispatch(originalProductlist(res));
      });
    },
  });

  const onAddToCartClick = () => {
    addToCart(item.id, 1)
      .then((res) => {
        enqueueSnackbar("yes baby", { variant: "success" });
      })
      .catch((err) => {
        enqueueSnackbar("fuck you bitch", { variant: "error" });
      });
  };
  const onDeleteCklick = (id) => {
    deleteProduct(id);
    const updatedProductList = productList.filter((item) => item.id != id);
    dispatch(catalogItemsRequestAction(updatedProductList));
  };

  const checked = (id) => {
    return checkedProducts.includes(id);
  };
  const openModal = (id) => {
    history.push(`/catalog/${id}`);
    dispatch(modalAction(true));
  };

  return (
    <>
      <Grid item sm={12} md={6} lg={4} xl={3} className="prod-wrapper">
        <div
          className={
            checked(item.id) ? " catalog__prod-active" : "catalog__prod"
          }
          id={classes.catalogProd}
          onClick={() => openModal(item.id)}
        >
          <div className="item_buttons">
            <input
              type="checkbox"
              className="prod-checkbox"
              onChange={() => selectItem(item.id)}
              checked={checked(item.id)}
              onClick={(e) => e.stopPropagation()}
            />
            <div className="admin_buttons-wrapper">
              <button className="admin_buttons">
                <DeleteIcon
                  className={
                    isAdmin ? classes.editIconActive : classes.editIcon
                  }
                  onClick={(e) => {
                    onDeleteCklick(item.id);
                    e.stopPropagation();
                  }}
                />
              </button>
              <button className="admin_buttons">
                <EditIcon
                  className={
                    isAdmin ? classes.editIconActive : classes.editIcon
                  }
                  onClick={(e) => {
                    handleClickOpen();
                    e.stopPropagation();
                  }}
                />
              </button>
            </div>
            <button
              onClick={(e) => {
                onAddToCartClick();
                e.stopPropagation();
              }}
              className="catalog__item-button"
              className="catalog__item-button"
            >
              Add To Inventory
            </button>
          </div>
          <div className="catalog__img">
            <img src={item.imageUrl} alt=""></img>
          </div>
          <div className="catalog__title">{item.title}</div>
          <div className="catalog__prices">
            <div className="catalog__price">
              <p className="price-number">${item.price}</p>
              <p className="price-p">RPG</p>
            </div>
            <div className="prod-line"></div>
            <div className="catalog__price">
              <p className="price-number">${item.price}</p>
              <p className="price-p">COST</p>
            </div>
            <div className="prod-line"></div>
            <div className="catalog__price">
              <p className="catalog__prod-profit">{item.price}%</p>
              <p className="price-p">PROFIT</p>
            </div>
          </div>
        </div>
      </Grid>
      <div
        className={open ? "edit_modal_wrapper-active" : "modal_open"}
        onClick={handleClose}
      >
        <form
          onClick={(e) => e.stopPropagation()}
          onSubmit={formik.handleSubmit}
          className="edit_add_modal"
        >
          <p className="form_title">Edit product</p>
          <div className="form_inputs-wrapper">
            <label className="form_label">Product title</label>
            <input
              className="add_form-input"
              id="title"
              name="title"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.title}
            ></input>
            <label className="form_label">Product description</label>
            <input
              className="add_form-input"
              id="desxription"
              name="description"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.description}
            ></input>
            <label className="form_label">Product price</label>
            <input
              className="add_form-input"
              id="price"
              name="price"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.price}
            ></input>
            <label className="form_label">Image url</label>
            <input
              className="add_form-input"
              id="imgUrl"
              name="imageUrl"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.imageUrl}
            ></input>
          </div>
          <button className="form_submit" type="submit">
            SUBMIT
          </button>
        </form>
      </div>
    </>
  );
};

export default CatalogItem;
