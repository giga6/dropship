import "./Modal.css";
import { useHistory, useParams } from "react-router-dom";
import { makeStyles, Modal } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getASingleProduct } from "../../../API";
import { modalAction } from "../../../redux/products/ProductActions";
import closeIcon from "../../../assets/close.png"
import { addToCart } from "../../../API";
import { useSnackbar } from "notistack";

const useStyles = makeStyles(() => ({
  productModal: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    border: "none",
  },
}));

const ProductModal = () => {
  const history = useHistory();
  const { productID } = useParams();
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const [openedProduct, setOpenedProduct] = useState({});
  const modalDisplayed = useSelector((state) => state.ProductReducers.modal);

  // getting product
  useEffect(() => {
    if (productID) {
      getASingleProduct(productID).then((res) => {
        setOpenedProduct(res);
      });
      console.log("sadfasfds");
    }
  }, [productID]);

  const close = () => {
    setOpenedProduct({});
    history.push("/catalog");
    dispatch(modalAction(false));
  };

  const onAddToCartClick = () => {
    addToCart(productID, 1)
      .then((res) => {
        enqueueSnackbar("yes baby", { variant: "success" });
      })
      .catch((err) => {
        enqueueSnackbar("fuck you bitch", { variant: "error" });
      });
  };

  return (
    <Modal
      className={classes.productModal}
      open={modalDisplayed ? modalDisplayed : false}
      onClose={close}
    >
      <>
        <div className={"modal__wrapper"}>
          <div className="modal__leftSide">
            {/* <ItemPricing price={openedProduct.price} /> */}
            <img src={openedProduct.imageUrl} className="modal__image" alt="" />
            <img
              src={openedProduct.imageUrl}
              className="modal__image--small"
              alt=""
            />
          </div>
          <div className="modal__rightSide">
            <div className="modal__itemId">
              <span className="modal__itemCode">SKU# frgx-459517 COPY</span>
              <span className="modal__supplier">
                Supplier: <a href="/#">SP-Supplier115</a>
              </span>
            </div>
            <h2 className="modal__itemTitle">{openedProduct.title}</h2>
            <div className="modal__buttonWrapper">
              <button onClick={onAddToCartClick} className="modal_inventory-button">Add To My Inventory</button>
            </div>
            <div className="modal__infoWrapper">
              <span>Product Details</span>
              <p className="modal__description">{openedProduct.description}</p>
            </div>
          </div>
          <img
            onClick={close}
            src={closeIcon}
            className="productModal__closeIcon"
            alt=""
          />
        </div>
      </>
    </Modal>
  );
};

export default ProductModal;
