import { useDispatch } from "react-redux";
import { useState } from "react";
import "./catalogHeaderBottom.css";
import {
  sortValueAction,
  catalogItemsRequestAction,
  originalProductlist,
} from "../../../redux/products/ProductActions";
import { useFormik } from "formik";
import { addProduct } from "../../../API";
import { products } from "../../../API";

const CatalogHeaderBottom = () => {
  const userInfo = JSON.parse(localStorage.getItem("user"));
  const isAdmin = userInfo?.data.data.isAdmin;
  const dispatch = useDispatch();
  const updateSortValue = (e) => {
    dispatch(sortValueAction(e.target.value));
  };
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const formik = useFormik({
    initialValues: {
      title: "",
      description: "",
      price: "",
      imageUrl: "",
    },
    onSubmit: async (values, { resetForm }) => {
      await addProduct(values);
      await products().then((res) => {
        dispatch(catalogItemsRequestAction(res));
        dispatch(originalProductlist(res));
      });
      await handleClose();
      await resetForm();
    },
  });
  return (
    <div className="catalog_header-bottom-wrapper">
      <div className="catalog_header-bottom">
        <div className="catalog_header-bottom-select-wrapper">
          <div className="catalog_header-bottom-select-icon">
            <div className="catalog_header-bottom-select-icon-line-1"></div>
            <div className="catalog_header-bottom-select-icon-line-2"></div>
            <div className="catalog_header-bottom-select-icon-line-3"></div>
          </div>
          <p>Sorted By</p>
          <select
            onChange={updateSortValue}
            className="catalog_header-bottom-select"
          >
            <option value="New Arrivals">New Arrivals</option>
            <option value="asc">Price: High To Low</option>
            <option value="desc">Price: Low To High</option>
            <option value="alphabet">Alphabet: High To Low</option>
            <option value="inAlphabet">Alphabet: Low To High</option>
          </select>
        </div>
      </div>
      <div className="addButtonWrapper">
        <button
          onClick={handleClickOpen}
          className={isAdmin ? "addButtonActive" : "addButton"}
        >
          ADD PRODUCT
        </button>
        <div
          className={open ? "modal_wrapper-active" : "modal_open"}
          onClick={handleClose}
        >
          <form
            onClick={(e) => e.stopPropagation()}
            onSubmit={formik.handleSubmit}
            className="add_modal"
          >
            <p className="form_title">Create new product</p>
            <div className="form_inputs-wrapper">
              <label className="form_label">Product title</label>
              <input
                className="add_form-input"
                id="title"
                name="title"
                type="text"
                onChange={formik.handleChange}
                value={formik.values.title}
              ></input>
              <label className="form_label">Product description</label>
              <input
                className="add_form-input"
                id="desxription"
                name="description"
                type="text"
                onChange={formik.handleChange}
                value={formik.values.description}
              ></input>
              <label className="form_label">Product price</label>
              <input
                className="add_form-input"
                id="price"
                name="price"
                type="text"
                onChange={formik.handleChange}
                value={formik.values.price}
              ></input>
              <label className="form_label">Image url</label>
              <input
                className="add_form-input"
                id="imgUrl"
                name="imageUrl"
                type="text"
                onChange={formik.handleChange}
                value={formik.values.imageUrl}
              ></input>
            </div>
            <button className="form_submit" type="submit">
              SUBMIT
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};
export default CatalogHeaderBottom;
