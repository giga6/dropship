import "./catalogHeaderTop.css";
import { useDispatch, useSelector } from "react-redux";
import seachIcon from "../../../assets/search-icon.png";
import { useEffect } from "react";
import { searchQuery } from "../../../redux/products/ProductActions";
import React from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import logo from "../../../assets/dropship_logo.png";
import profileIcon from "../../../assets/profile-example.jpg";
import dashboarIcon from "../../../assets/dashboard.svg";
import catalogIcon from "../../../assets/catalog.svg";
import inventoryIcon from "../../../assets/inventory.svg";
import cartIcon from "../../../assets/cart.svg";
import orderIcon from "../../../assets/order.svg";
import transactionsIcon from "../../../assets/transactions.svg";
import storeIcon from "../../../assets/store.svg";
import MenuIcon from "@material-ui/icons/Menu";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  fullList: {
    width: "auto",
  },

  burgerButton: {
    borderBottom: "none !important",
  },
  burgerList: {
    marginTop: "-70px !important",
  },
});

const CatalogHeaderTop = ({ search, checkedProducts, selectAll, clear }) => {
  const dispatch = useDispatch();
  const productList = useSelector(
    (state) => state.ProductReducers.catalogProductsList
  );
  const updateSearchQuery = (e) => {
    dispatch(searchQuery(e.target.value));
  };
  const classes = useStyles();
  const matches = useMediaQuery("(max-width:1070px)");
  const [state, setState] = React.useState({
    top: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === "top" || anchor === "bottom",
      })}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        <nav className="burger_nav">
          <div className="burger_logo">
            <img alt="logo" src={logo}></img>
          </div>
          <ul className="burger_ul">
            <Link to="/profile">
              <li className="profile_icon">
                <img alt="profileIcon" src={profileIcon}></img>
              </li>
            </Link>
            <Link to="/dashboard">
              <li className="aside_icon">
                <img alt="dashboarIcon" src={dashboarIcon}></img>
              </li>
            </Link>
            <Link to="/catalog">
              <li className="aside_icon">
                <img alt="catalogIcon" src={catalogIcon}></img>
              </li>
            </Link>
            <Link to="/inventory">
              <li className="aside_icon">
                <img alt="inventoryIcon" src={inventoryIcon}></img>
              </li>
            </Link>
            <Link to="/cart">
              <li className="aside_icon">
                <img alt="cartIcon" src={cartIcon}></img>
              </li>
            </Link>
            <Link to="/order">
              <li className="aside_icon">
                <img src={orderIcon}></img>
              </li>
            </Link>
            <Link to="/transactions">
              <li className="aside_icon">
                <img alt="transactionsIcon" src={transactionsIcon}></img>
              </li>
            </Link>
            <Link to="/store">
              <li className="aside_icon">
                <img alt="storeIcon" src={storeIcon}></img>
              </li>
            </Link>
          </ul>
        </nav>
      </List>
    </div>
  );

  return (
    <div className="header_wrapper">
      <div className="header">
        <div className="header_left">
          <button onClick={selectAll} className="header_selectAll-button">
            SELECT ALL
          </button>
          <div className="header_left-line"></div>
          <div className="selected_items-wrapper">
            <p className="selected_p">
              sellected {checkedProducts.length} out of &nbsp;
            </p>
            <p className="all_products_p">{productList.length} products</p>
          </div>
          <div className="clear__wrapper">
            <button
              onClick={clear}
              className={
                checkedProducts.length > 0
                  ? "header_clear-button"
                  : "header_clear-button-none"
              }
            >
              clear
            </button>
          </div>
        </div>
        <div className="header_right">
          <div className="search_input-wrapper">
            <div className="search_input-div">
              <input
                onChange={updateSearchQuery}
                className="search_input"
              ></input>
              <button onClick={search} className="search_button">
                <img alt="" src={seachIcon}></img>
              </button>
            </div>
          </div>
          <button className="header_inventory-button">ADD TO INVENTORY</button>
          <div>
            {["right"].map((anchor) => (
              <React.Fragment key={anchor}>
                {matches && (
                  <Button
                    className={classes.burgerbutton}
                    onClick={toggleDrawer(anchor, true)}
                  >
                    <MenuIcon className={classes.burger}></MenuIcon>
                  </Button>
                )}
                <Drawer
                  className={classes.burgerList}
                  anchor={anchor}
                  open={state[anchor]}
                  onClose={toggleDrawer(anchor, false)}
                >
                  {list(anchor)}
                </Drawer>
              </React.Fragment>
            ))}
          </div>
          <button className="help_button">?</button>
        </div>
      </div>
    </div>
  );
};

export default CatalogHeaderTop;
