import "./catalog.css";
import CatalogHeaderTop from "./catalogHeaderTop";
import CatalogHeaderBottom from "./catalogHeaderBottom";
import { Grid } from "@material-ui/core";
import CatalogItem from "./catalogItem";
import { useDispatch, useSelector } from "react-redux";
import {
  productSearch,
  productsSortAction,
  catalogItemsRequestAction,
  checkedProductsAction,
  checkProductAction,
  uncheckAction,
} from "../../redux/products/ProductActions";
import { useEffect } from "react";
import ProductModal from "./catalogItem/Modal";

const Catalog = () => {
  const sortValue = useSelector((state) => state.ProductReducers.sortValue);
  const searchQuery = useSelector((state) => state.ProductReducers.searchQuery);
  const prices = useSelector((state) => state.ProductReducers.filterPrice);
  const dispatch = useDispatch();
  const originalProductlist = useSelector(
    (state) => state.ProductReducers.originalProducts
  );
  const productList = useSelector(
    (state) => state.ProductReducers.catalogProductsList
  );
  const checkedProducts = useSelector(
    (state) => state.ProductReducers.checkedProducts
  );

  const search = () => {
    const searchProducts = productList.filter((item) =>
      item.title.includes(searchQuery)
    );
    dispatch(productSearch(searchQuery ? searchProducts : originalProductlist));
  };

  const sort = () => {
    let updatedItems = [...productList];
    if (sortValue === "desc") {
      updatedItems.sort((a, b) => b.price - a.price);
    }
    if (sortValue === "asc") {
      updatedItems.sort((a, b) => a.price - b.price);
    }
    if (sortValue === "inAlphabet") {
      updatedItems.sort((a, b) => {
        if (a.title > b.title) {
          return -1;
        }
      });
    } else if (sortValue === "alphabet") {
      updatedItems.sort((a, b) => {
        if (a.title < b.title) {
          return -1;
        }
      });
    }

    return updatedItems;
  };

  useEffect(() => {
    if (sortValue) {
      const sortedItems = sort(sortValue);
      dispatch(productsSortAction(sortedItems));
    }
  }, [sortValue]);

  const filter = (products) => {
    let filteredProducts = products.filter(
      (item) => item.price <= prices[1] && item.price >= prices[0]
    );
    dispatch(catalogItemsRequestAction(filteredProducts));
  };

  useEffect(() => {
    if (originalProductlist) {
      setTimeout(() => {
        filter(originalProductlist);
      }, 500);
    }
  }, [prices]);

  const selectItem = (id) => {
    const checked = checkedProducts.findIndex((item) => item === id);
    if (checked > -1) {
      dispatch(uncheckAction(id));
    } else {
      dispatch(checkProductAction(id));
    }
  };
  const selectAll = () => {
    const productId = originalProductlist.map((item) => {
      return (item = item.id);
    });
    dispatch(checkedProductsAction(productId));
  };
  const clear = () => {
    dispatch(checkedProductsAction([]));
  };

  return (
    <div className="catalog">
      <ProductModal />
      <CatalogHeaderTop
        search={search}
        checkedProducts={checkedProducts}
        selectAll={selectAll}
        clear={clear}
      />
      <CatalogHeaderBottom />
      <Grid container xs={12} spacing={3} className="catalog__list">
        {productList.map((item) => (
          <CatalogItem
            key={item.id}
            productList={productList}
            item={item}
            selectItem={selectItem}
          />
        ))}
      </Grid>
    </div>
  );
};
export default Catalog;
