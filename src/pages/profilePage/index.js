import Aside from "../../components/common/aside";
import Profile from "./profile";
import "./profilePage.css";
const ProfilePage = () => {
  return (
    <div className="profile__page-wrapper">
      <Aside />
      <Profile />
    </div>
  );
};

export default ProfilePage;
