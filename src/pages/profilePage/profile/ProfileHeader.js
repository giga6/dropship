import "./profileHeader.css";
import Button from "../../../components/common/button";
import { useHistory } from "react-router";
const ProfileHeader = () => {
  const history = useHistory();
  const onSignOutClick = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    history.push("/");
  };
  return (
    <header className="profile__header">
      <div className="sub_header">
        <div className="profile__title-wrapper">
          <p className="profile_title"> MY PROFILE</p>
        </div>
        <div className="profile_buttons_wrapper">
          <button onClick={onSignOutClick} className="profile__sign-out">
            SIGN OUT
          </button>
          <Button title="?" className="help_button" />
        </div>
      </div>
    </header>
  );
};

export default ProfileHeader;
