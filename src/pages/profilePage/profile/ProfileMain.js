import Button from "../../../components/common/button";
import "./ProfileMain.css";
import profileIcon from "../../../assets/profile-example.jpg";
import CountrySelect from "../../../components/common/countySelect";
const ProfileMain = () => {
  return (
    <section className="profile_section">
      <header className="profile__section-header">
        <nav className="profile__section-nav">
          <ul className="profile__section-ul">
            <li className="profile__section-li">PROFILE</li>
            <li className="profile__section-li">BILLING</li>
            <li className="profile__section-li">INVOICE HISTORY</li>
          </ul>
          <Button title="DEACTIVATE ACCOUNT" className="profile_deactivate" />
        </nav>
      </header>
      <main className="profile_section-main">
        <section className="profile_main-section">
          <p className="profile_main-section-title">PROFILE PICTURE</p>
          <div className="profile_main-content">
            <div className="profile_pricture_div">
              <img src={profileIcon}></img>
            </div>
            <Button title="Upload" className="upload__button" />
          </div>
        </section>
        <section className="profile_main-section">
          <p className="profile_main-section-title">PERSONAL DETAILS</p>
          <div className="profile_main-content">
            <form className="password_form">
              <label className="form_label">First Name</label>
              <input className="form-input"></input>
              <label className="form_label">Last Name</label>
              <input className="form-input"></input>
              <label className="form_label">Country</label>
              <CountrySelect />
            </form>
          </div>
        </section>
        <section className="profile_main-section">
          <p className="profile_main-section-title">CHANGE PASSWORD</p>
          <div className="profile_main-content">
            <form className="password_form">
              <label className="form_label">Current Password</label>
              <input className="form-input"></input>
              <label className="form_label">New Password</label>
              <input className="form-input"></input>
              <label className="form_label">Confirm New Password</label>
              <input className="form-input"></input>
            </form>
          </div>
        </section>
        <section className="profile_main-section">
          <p className="profile_main-section-title">CONTACT INFORMATION</p>
          <div className="profile_main-content">
            <form className="password_form">
              <label className="form_label">Email</label>
              <input className="form-input"></input>
              <label className="form_label">Skype</label>
              <input className="form-input"></input>
              <label className="form_label">Phone</label>
              <input className="form-input"></input>
            </form>
          </div>
        </section>
      </main>
    </section>
  );
};

export default ProfileMain;
