import ProfileHeader from "./ProfileHeader";
import ProfileMain from "./ProfileMain";
import "./profile.css";
const Profile = () => {
  return (
    <div className="profile">
      <ProfileHeader />
      <ProfileMain />
    </div>
  );
};

export default Profile;
