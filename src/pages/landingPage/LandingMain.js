import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";

const LandingMain = () => {
  return (
    <main className="landing-main">
      <div className="landing-banner-wrapper">
        <div className="landing-content">
          <div className="landing-banner">
            <div className="landing-banner-logo">
              <img
                alt=""
                src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/356Logo.svg"
              ></img>
            </div>
            <div className="landing-text">
              <p className="landing-banner-p">
                WE GOT YOUR SUPPLY CHAIN COVERED
              </p>
              <p className="landing-banner-p">365 DAYS A YEAR!</p>
            </div>
          </div>
          <div className="landing__banner_button-wrapper">
            <Link to="/signup" style={{ textDecoration: "none" }}>
              <Button
                style={{ width: "175px", height: "50px" }}
                variant="contained"
                size="large"
                color="primary"
              >
                Sign up
              </Button>
            </Link>
          </div>
        </div>
      </div>
    </main>
  );
};

export default LandingMain;
