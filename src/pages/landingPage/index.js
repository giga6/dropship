import React from 'react';
import LandingHeader from "./LandingHeader"
import LandingMain from "./LandingMain"
import './Landing.css'

const LandingPage = () =>{
    return(
        <div className="landing-wrapper">
            < LandingHeader  />
            < LandingMain />
        </div>    
    )
}

export default LandingPage