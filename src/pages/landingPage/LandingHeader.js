import { Button } from "@material-ui/core";
import HomeLogo from "../../assets/dropship-home-logo.png";
import { Link } from "react-router-dom";
import Burger from "../../components/common/burger";

const LandingHeader = () => {
  const burgerList = [
    "ABOUT",
    "CATALOG",
    "PRICING",
    "SUPPLIERS",
    "HELP CENTER",
    "BLOG",
    "SIGN UP",
    "LOGIN",
  ];
  const token = localStorage.getItem("token");
  return (
    <header className="landing__header">
      <nav className="landing__header-nav">
        <div className="landing__header-logo">
          <img alt="" src={HomeLogo}></img>
        </div>
        <ul className="landing__header-ul">
          <li className="landing__header-li">ABOUT</li>
          <Link
            style={{ textDecoration: "none" }}
            to={token ? "/catalog" : "/"}
          >
            <li className="landing__header-li">CATALOG</li>
          </Link>
          <li className="landing__header-li">PRICING</li>
          <li className="landing__header-li">SUPPLIERS</li>
          <li className="landing__header-li">HELP CENTER</li>
          <li className="landing__header-li">BLOG</li>
          <li className="landing__header-li">
            <Link style={{ textDecoration: "none" }} to="/signup">
              <Button variant="outlined" color="primary">
                SIGN UP NOW
              </Button>
            </Link>
          </li>
          <Link style={{ textDecoration: "none" }} to="/login">
            <Button color="primary" className="landing__header-li">
              LOGIN
            </Button>
          </Link>
          <li className="landing__header-li">
            <img alt=""></img>
          </li>
        </ul>
        <Burger burgerList={burgerList} />
      </nav>
    </header>
  );
};

export default LandingHeader;
