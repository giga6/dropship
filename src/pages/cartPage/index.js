import "./cartPage.css";
import Aside from "../../components/common/aside";
import Cart from "./cart/Cart";
import { cartProductsListAction } from "../../redux/products/ProductActions";
import { cartProducts } from "../../API";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import CartHeader from "./cart/CartHeader";

const CartPage = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    cartProducts().then((res) => {
      dispatch(cartProductsListAction(res));
    });
  }, []);
  return (
    <div className="cart_page-wrapper">
      <Aside />
      <CartHeader productsLength={cartProducts.length} />
      <Cart />
    </div>
  );
};
export default CartPage;
