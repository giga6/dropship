import { useDispatch, useSelector } from "react-redux";
import "./cart.css";
import CartItem from "./cartProducts/CartItem";
import { removeFromCart } from "../../../API";
import { cartProductsListAction } from "../../../redux/products/ProductActions";
import CartProductsHeader from "./cartProducts/CartProductsHeader";
import { useSnackbar } from "notistack";
const Cart = () => {
  const { enqueueSnackbar } = useSnackbar();
  const dispatch = useDispatch();
  const cartProducts =
    useSelector((State) => State.ProductReducers.cartProductsList) || [];
  const onDeleteClick = (id) => {
    removeFromCart(id)
      .then((res) => {
        const filterdCartProducts = cartProducts.filter((item) => {
          return item.id !== id;
        });
        dispatch(cartProductsListAction(filterdCartProducts));
        enqueueSnackbar("you are tesli babyyyyy", { variant: "success" });
      })
      .catch((err) => {
        enqueueSnackbar("you are sirii babyyyyy", { variant: "error" });
      });
  };

  return (
    <div className="cart_wrapper">
      <section className="cart_prpducts">
        <CartProductsHeader />
        {cartProducts.map((item) => (
          <CartItem key={item.id} item={item} onDeleteClick={onDeleteClick} />
        ))}
      </section>
    </div>
  );
};
export default Cart;
