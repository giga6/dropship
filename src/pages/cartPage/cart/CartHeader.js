import "./cartHeader.css";
import { useSelector } from "react-redux";
const CartHeader = () => {
  const cartProducts = useSelector(
    (state) => state.ProductReducers.cartProductsList
  );
  return (
    <header className="cart__header">
      <p className="cart__header-title">
        SHOPPING CART ({cartProducts && cartProducts.length})
      </p>
      <button className="help_button">?</button>
    </header>
  );
};
export default CartHeader;
