import "./cartProductsHeader.css";
const CartProductsHeader = () => {
  return (
    <header className="cart_product-header">
      <nav className="cart_product-nav">
        <ul className="cart_product-ul">
          <li className="cart_product-description-li">ITEM DESCRIPTION</li>
          <li className="cart_product-quantity-li">QUANTITY</li>
          <li className="cart_product-cost-li">ITEM COST</li>
        </ul>
      </nav>
    </header>
  );
};

export default CartProductsHeader;
