import "./cartItem.css";
import DeleteIcon from "@material-ui/icons/Delete";
import { makeStyles } from "@material-ui/core/styles";
const useStyles = makeStyles({
  deleteButton: {
    color: "#f44336",
  },
});
const CartItem = ({ item, onDeleteClick }) => {
  const classes = useStyles();

  return (
    <div className="cart__item-wrapper">
      <div className="cartItem">
        <div className="cart__item-description">
          <img src={item.image}></img>
          <p>{item.title}</p>
        </div>
        <div className="cart__item-quantity-wraaper">
          <p>{item.qty}</p>
        </div>
        <div className="cart__item-cost">
          <p>{item.price}$</p>
        </div>
        <div className="cart__item-delete">
          <button
            onClick={() => {
              onDeleteClick(item.id);
            }}
            className="delete__button"
          >
            <DeleteIcon className={classes.deleteButton} />
          </button>
        </div>
      </div>
    </div>
  );
};

export default CartItem;
