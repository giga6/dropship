import React, { useState } from "react";
import "./catalogPage.css";
import Aside from "../../components/common/aside";
import Sidebar from "../../components/common/sidebar";
import Catalog from "../../components/catalog";
import { products } from "../../API";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { catalogItemsRequestAction } from "../../redux/products/ProductActions";
import {
  originalProductlist,
  filterPriceAction,
} from "../../redux/products/ProductActions";
const CatalogPage = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    products().then((res) => {
      dispatch(catalogItemsRequestAction(res));
      dispatch(originalProductlist(res));
      const prices = res.map((item) => item.price);
      const minMax = [Math.min(...prices), Math.max(...prices)];
      dispatch(filterPriceAction(minMax));
    });
  }, []);

  return (
    <div className="catalogPage_wrapper">
      <Aside />
      <Sidebar />
      <Catalog />
    </div>
  );
};

export default CatalogPage;
