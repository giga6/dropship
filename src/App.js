import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { createTheme, ThemeProvider } from "@material-ui/core/styles";
import LandingPage from "./pages/landingPage";
import SignUp from "./components/SignUp-LogIn/Signup";
import LogIn from "./components/SignUp-LogIn/Login";
import CatalogPage from "./pages/catalogPage";
import ProfilePage from "./pages/profilePage";
import CartPage from "./pages/cartPage";
const theme = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1480,
      xl: 1800,
    },
  },
  palette: {
    primary: {
      main: "#61d5df",
      contrastText: "#FFFFFF",
    },
    secondary: {
      main: "#5d6b9f",
    },
    error: {
      main: "#f44336",
    },
  },
});
function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Switch>
          <Route exact path="/">
            <LandingPage />
          </Route>
          <Route exact path="/signup">
            <SignUp />
          </Route>
          <Route exact path="/login">
            <LogIn />
          </Route>
          <Route exact path="/profile">
            <ProfilePage />
          </Route>
          <Route exact path="/catalog/:productID?">
            <CatalogPage />
          </Route>
          <Route exact path="/cart">
            <CartPage />
          </Route>
        </Switch>
      </Router>
    </ThemeProvider>
  );
}

export default App;
